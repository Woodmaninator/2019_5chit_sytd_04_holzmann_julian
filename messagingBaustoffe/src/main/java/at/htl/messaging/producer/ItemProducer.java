package at.htl.messaging.producer;

import at.htl.messaging.model.Item;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class ItemProducer {
        @Autowired
        private ObjectMapper objectMapper;
        @Autowired
        private RabbitTemplate rabbitTemplate;
        public void sendItem(Item item) throws JsonProcessingException {
            String message = objectMapper.writeValueAsString(item);
            rabbitTemplate.convertAndSend("htl.pc.itemExchange", item.getCategoryName(), message);
            rabbitTemplate.convertAndSend("htl.pc.itemExchange", "Archive", message);
        }
}
