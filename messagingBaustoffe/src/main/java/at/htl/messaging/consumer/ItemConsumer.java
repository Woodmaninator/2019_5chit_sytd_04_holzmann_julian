package at.htl.messaging.consumer;

import at.htl.messaging.model.Item;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemConsumer {
    private static final Logger logger = LoggerFactory.getLogger(ItemConsumer.class);
    @Autowired
    private ObjectMapper objectMapper;
    @RabbitListener(queues = {"htl.pc.itemQueueFliesen","htl.pc.itemQueueBoeden","htl.pc.itemQueueBaustoffe"})
    public void listenItem(String message) throws JsonProcessingException {
        Item item = objectMapper.readValue(message, Item.class);
        logger.info("Received Item: {}",item.toString());
    }
    @RabbitListener(queues = "htl.pc.itemQueueArchive")
    public void listenItemArchive(String message) throws JsonProcessingException {
        Item item = objectMapper.readValue(message, Item.class);
        logger.info("Received Item to archive: {}",item.toString());
    }
}
