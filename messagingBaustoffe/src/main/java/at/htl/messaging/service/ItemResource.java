package at.htl.messaging.service;

import at.htl.messaging.model.Item;
import at.htl.messaging.producer.ItemProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/item")
public class ItemResource {
    @Autowired
    private ItemProducer itemProducer;
    @PostMapping(path="/sendItem")
    @ResponseStatus(HttpStatus.OK)
    public void approveProject(@RequestBody @Valid Item item) throws JsonProcessingException {
        itemProducer.sendItem(item);
    }
}
