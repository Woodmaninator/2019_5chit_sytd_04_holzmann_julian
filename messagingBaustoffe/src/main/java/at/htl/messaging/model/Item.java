package at.htl.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.annotation.sql.DataSourceDefinition;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    private String name;
    private String categoryName;
    private String description;
}
