package at.htl.messaging;

import at.htl.messaging.model.Item;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class ItemIntegrationTests {
    @Test
    public void sendProjectTest() throws JsonProcessingException {
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","buildingMat","item/sendItem");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        Item testItem = new Item("Fliese 1", "Fliesen", "San hoit fliesen, oder?");
        HttpEntity<Item> requestData = new HttpEntity<Item>(testItem, httpHeaders);
        RestTemplate restClient = new RestTemplate();
        restClient.postForEntity(requestURL, requestData, Void.class);
    }
}
