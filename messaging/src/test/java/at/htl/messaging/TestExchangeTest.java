package at.htl.messaging;

import at.htl.messaging.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class TestExchangeTest {
    @Test
    public void sendMessageTest() throws JsonProcessingException {

        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","message","testExchange");

        RestTemplate restClient = new RestTemplate();

        restClient.getForEntity(requestURL, Void.class);
    }
}
