package at.htl.messaging;

import at.htl.messaging.message.consumer.DirectRabbitMQConsumer;
import at.htl.messaging.message.producer.DirectRabbitMQProducer;
import at.htl.messaging.message.producer.ProjectProducer;
import at.htl.messaging.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class ProjectIntegrationTest {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    @Autowired
    private ProjectProducer producer;

    @Test
    public void sendProjectTest() throws JsonProcessingException {

        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","message","project/approvement/init");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        Facility facility = new Facility("science");

        //Definition eines Testprojekts, das an den Service geschickt werden soll
        ManagementProject testProject = new ManagementProject(facility, "Project Title", "Project Description", EProjectState.CREATED, ELawType.P_27);

        HttpEntity<AProject> requestData = new HttpEntity<AProject>(testProject, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        logger.info(requestData.toString());
        //Schicken des Requests
        restClient.postForEntity(requestURL, requestData, Void.class);
    }
}