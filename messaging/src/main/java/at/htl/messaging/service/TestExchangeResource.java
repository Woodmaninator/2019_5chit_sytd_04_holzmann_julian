package at.htl.messaging.service;

import at.htl.messaging.message.producer.DirectRabbitMQProducer;
import at.htl.messaging.message.producer.testExchanges.DirectMessageProducer;
import at.htl.messaging.message.producer.testExchanges.FanoutMessageProducer;
import at.htl.messaging.message.producer.testExchanges.HeaderMessageProducer;
import at.htl.messaging.message.producer.testExchanges.TopicMessageProducer;
import at.htl.messaging.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/testExchange")
public class TestExchangeResource {
    @Autowired
    private DirectMessageProducer directMessageProducer;

    @Autowired
    private FanoutMessageProducer fanoutMessageProducer;

    @Autowired
    private HeaderMessageProducer headerMessageProducer;

    @Autowired
    private TopicMessageProducer topicMessageProducer;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public void testExchange() {
        directMessageProducer.sendMessage("Direct Test");
        fanoutMessageProducer.sendMessage("Fanout Test");
        topicMessageProducer.sendMessage("Topic Test");
        headerMessageProducer.sendMessage("Header Test");
    }
}
