package at.htl.messaging.service;

import at.htl.messaging.message.consumer.DirectRabbitMQConsumer;
import at.htl.messaging.message.producer.ProjectProducer;
import at.htl.messaging.model.AProject;
import at.htl.messaging.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//Definiert, dass es sich bei der Klasse um einen REST Controller handelt
@RestController
//Definiert den Teil der URL der Requests, der nach dem URL Teil in der Properties Datei folgt
@RequestMapping(path="/project")
public class ProjectResource {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    @Autowired
    private ProjectProducer projectProducer;

    //Definiert POST Mapping für die URL /approvement/init (folgt nach dem Teil, der über der Klasse definiert wurde)
    @PostMapping(path="/approvement/init")
    //Definiert, dass bei erfolgreicher Ausführung der Methode der HTTP Statuscode 200 OK zurückgegeben wird
    @ResponseStatus(HttpStatus.OK)
    //Definiert, dass der Body des HTTP Requests vom Typ Aproject sein muss
    public void approveProject(@RequestBody @Valid AProject project) throws JsonProcessingException {
        //Überprüft den Projektstatus und leitet es demensptrechend an den richtigen Message Producer weiter
        if(project.getProjectState() == EProjectState.CREATED) {
            logger.info("Project State = CREATED");
            //Weiterleitung an ProjectProducer
            projectProducer.sendProject(project);
        } else {
            //400 Bad Request, sagt uns da panhofer noch wie der dreck geht
            logger.info("Project State = NOT CREATED");
        }
    }
}