package at.htl.messaging.service;

import at.htl.messaging.message.producer.DirectRabbitMQProducer;
import at.htl.messaging.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

@RestController
@RequestMapping(path="/employee")
public class MessagingResource implements Serializable {

    @Autowired
    private DirectRabbitMQProducer producer;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void getEmployee(@RequestBody Employee employee) throws JsonProcessingException {
        producer.sendDirectMessage(employee);
    }
}
