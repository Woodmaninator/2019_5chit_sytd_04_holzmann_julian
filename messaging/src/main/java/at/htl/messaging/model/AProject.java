package at.htl.messaging.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = JsonTypeInfo.As.PROPERTY,property="json-type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ManagementProject.class, name = "managementProject"),
        @JsonSubTypes.Type(value = ReqeustFundingProject.class, name = "reqeustFondingProject")
})
public abstract class AProject implements Serializable {
    private Facility facility;

    private String title;

    private String description;

    private EProjectState projectState;

    public AProject(Facility facility, String title, String description, EProjectState projectState){
        this.facility = facility;
        this.title = title;
        this.description = description;
        this.projectState = projectState;
    }
}
