package at.htl.messaging.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class ManagementProject extends AProject {
    private ELawType lawType;
    public ManagementProject(Facility facility, String title, String description, EProjectState projectState, ELawType eLawType){
        super(facility,title,description,projectState);
        this.lawType = eLawType;
    }
}
