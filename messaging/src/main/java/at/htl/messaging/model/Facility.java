package at.htl.messaging.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.corba.se.impl.legacy.connection.SocketFactoryContactInfoListIteratorImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
public class Facility implements Serializable {
    private String facilityName;
    public Facility(String facilityName){
        this.facilityName=facilityName;
    }
}
