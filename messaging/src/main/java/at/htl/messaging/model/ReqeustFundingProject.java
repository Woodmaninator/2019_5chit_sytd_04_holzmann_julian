package at.htl.messaging.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class ReqeustFundingProject extends AProject {
    private Boolean isFFWFunded;
    private Boolean isFFGFunded;
    private Boolean isEUFunded;
}
