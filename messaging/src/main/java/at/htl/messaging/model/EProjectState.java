package at.htl.messaging.model;

public enum EProjectState {
    CREATED, IN_APPROVEMENT, CANCELLED, APPROVED
}
