package at.htl.messaging.message.producer.testExchanges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TopicMessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.producer.testExchanges.TopicMessageProducer.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String message) {
        rabbitTemplate.convertAndSend("htl.pc.testExchangeTopic","q1.test", message);
        logger.info("Topic Producer sent: " + message);
    }
}
