package at.htl.messaging.message.consumer.testExchanges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class HeaderMessageConsumer {
    private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.consumer.testExchanges.HeaderMessageConsumer.class);

    @RabbitListener(queues = "htl.pc.testQueueHeader")
    public void listen(String message) {
        logger.info("Header Message Received:" + message);
    }
}
