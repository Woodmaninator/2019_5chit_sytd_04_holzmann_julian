package at.htl.messaging.message.consumer;

import at.htl.messaging.model.AProject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectResultConsumer {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = {"htl.pc.projectResultQueueAccepted","htl.pc.projectResultQueueCancelled"})
    public void listenProjectResultAccepted(String message) throws JsonProcessingException {
        AProject project = objectMapper.readValue(message, AProject.class);
        logger.info("Received Result Project: {}", project.toString());
    }

//    @RabbitListener(queues = "htl.pc.projectResultQueueCancelled")
//    public void listenProjectResultCancelled(String message) throws JsonProcessingException {
//        AProject project = objectMapper.readValue(message, AProject.class);
//        logger.info("Received Cancelled Project: {}",project.toString());
//    }
}
