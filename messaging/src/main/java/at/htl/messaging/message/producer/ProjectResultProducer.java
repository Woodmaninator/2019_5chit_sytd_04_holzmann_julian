package at.htl.messaging.message.producer;

import at.htl.messaging.model.AProject;
import at.htl.messaging.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectResultProducer {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProjectResult(AProject project) throws JsonProcessingException {
        String queueName = "";
        if(project.getProjectState() == EProjectState.APPROVED)
            queueName = "approved";
        if(project.getProjectState() == EProjectState.CANCELLED)
            queueName = "cancelled";

        String message = objectMapper.writeValueAsString(project);

        rabbitTemplate.convertAndSend("htl.pc.projectResultExchange", queueName, message);
    }
}
