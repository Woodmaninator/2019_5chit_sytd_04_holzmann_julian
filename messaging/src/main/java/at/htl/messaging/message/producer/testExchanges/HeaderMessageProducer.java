package at.htl.messaging.message.producer.testExchanges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HeaderMessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.producer.testExchanges.HeaderMessageProducer.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String message) {
        rabbitTemplate.setExchange("htl.pc.testExchangeHeader");
        rabbitTemplate.convertAndSend("" , message, x -> {
            x.getMessageProperties().getHeaders().put("key1", "q1");
            return x;
        });
        logger.info("Header Producer sent: " + message);
    }
}
