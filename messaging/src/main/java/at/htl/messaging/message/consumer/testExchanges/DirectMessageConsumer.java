package at.htl.messaging.message.consumer.testExchanges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class DirectMessageConsumer {
     private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.consumer.testExchanges.DirectMessageConsumer.class);

     @RabbitListener(queues = "htl.pc.testQueueDirect")
     public void listen(String message) {
         logger.info("Direct Message Received:" + message);
     }
}
