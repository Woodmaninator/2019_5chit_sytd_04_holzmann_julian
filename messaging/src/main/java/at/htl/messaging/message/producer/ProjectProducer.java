package at.htl.messaging.message.producer;

import at.htl.messaging.message.consumer.DirectRabbitMQConsumer;
import at.htl.messaging.model.AProject;
import at.htl.messaging.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

//Definiert, dass es sich bei dieser Klasse um ein CDI Bean handelt und es somit möglich ist, bei deiser Klasse die
//@Autowired Annotation zu verwenden
//Anmerkung: CDI Beans sind, wenn es nicht anders definiert ist, Singletons
@Component
public class ProjectProducer {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    //Instanz der ObjectMapper Klasse, die benötigt wird, um Objekte zu JSON zu parsen
    @Autowired
    private ObjectMapper objectMapper;
    //Instanz der RabbitTemplate Klasse, die benötigt wird, um JSON an einen bestimmten Exchange mit einem bestimmten Routing Key zu senden
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProject(AProject project) throws JsonProcessingException {
        //Projekt wird mit einer Wahrscheinlichkeit von 20% abgelehnt bzw. zu 80% akzeptiert.
        //Hier erfolgt das Setzen des Projektstatus
        Random davidStinkt = new Random();
        if(davidStinkt.nextInt(6) == 1)
            project.setProjectState(EProjectState.CANCELLED);
        else
            project.setProjectState(EProjectState.APPROVED);
        //JSON Parsing
        String message = objectMapper.writeValueAsString(project);
        //Sendet den JSON String an den Exchange htl.pc.projectExchange mit dem Facility Namen als Routing Key
        rabbitTemplate.convertAndSend("htl.pc.projectExchange",project.getFacility().getFacilityName(),message);
    }
}
