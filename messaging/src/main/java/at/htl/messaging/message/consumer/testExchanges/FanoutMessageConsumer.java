package at.htl.messaging.message.consumer.testExchanges;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class FanoutMessageConsumer {
    private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.consumer.testExchanges.FanoutMessageConsumer.class);

    @RabbitListener(queues = "htl.pc.testQueueFanout")
    public void listen(String message) {
        logger.info("Fanout Message Received:" + message);
    }
}
