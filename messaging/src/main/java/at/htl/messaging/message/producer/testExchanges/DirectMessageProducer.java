package at.htl.messaging.message.producer.testExchanges;

import at.htl.messaging.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DirectMessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(at.htl.messaging.message.producer.testExchanges.DirectMessageProducer.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String message) {
        rabbitTemplate.convertAndSend("htl.pc.testExchangeDirect","q1", message);
        logger.info("Direct Producer sent: " + message);
    }
}
