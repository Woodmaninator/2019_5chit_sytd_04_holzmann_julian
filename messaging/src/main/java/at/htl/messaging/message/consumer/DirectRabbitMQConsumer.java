package at.htl.messaging.message.consumer;

import at.htl.messaging.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

@Service
public class DirectRabbitMQConsumer {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = "htl.pc.queue1")
    public void listen(String message) throws JsonProcessingException {

        Employee employee = objectMapper.readValue(message, Employee.class);
        logger.info("EmployeeObject: {}",employee.toString());
    }
}
