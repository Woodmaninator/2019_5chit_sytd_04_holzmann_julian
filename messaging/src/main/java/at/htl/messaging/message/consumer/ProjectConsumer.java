package at.htl.messaging.message.consumer;


import at.htl.messaging.message.producer.ProjectResultProducer;
import at.htl.messaging.model.AProject;
import at.htl.messaging.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Macht Klasse zu CDI Bean
//@Service Annotation wird bei Klassen verwendet, die Logik zur Verfügung stellen (in diesem Fall auslesen von Queues)
//Consumer ist dafür verantwortlich, die Messages, die sich in der Queue befinden, auszulesen und entsprechend zu verarbeiten
@Service
public class ProjectConsumer {
    private static final Logger logger = LoggerFactory.getLogger(DirectRabbitMQConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;
    //Producer, der die Message an den nächsten Exchange sendet
    @Autowired
    private ProjectResultProducer producer;

    //Definiert die Queues, die die Methode "abhört"
    //Es ist möglich, für die gleiche Methode mehrere Queues anzugeben
    @RabbitListener(queues = {"htl.pc.projectQueueScience","htl.pc.projectQueueArchitecture","htl.pc.projectQueueElectricity"})
    public void listenScienceProject(String message) throws JsonProcessingException {
        //JSON Parsing zur Klasse
        AProject project = objectMapper.readValue(message, AProject.class);
        logger.info("Received Project: {}", project.toString());
        //Weiterleitung an den nächsten Producer
        producer.sendProjectResult(project);
    }
}
