package at.htl.aopproject.model.beans;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
public abstract class AProject implements Serializable {

    @GeneratedValue
    @Id
    private Long id;

    private String title;

    private String description;

}
