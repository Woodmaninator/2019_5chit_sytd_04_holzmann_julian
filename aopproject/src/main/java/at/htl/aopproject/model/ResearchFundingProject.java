package at.htl.aopproject.model;

import at.htl.aopproject.model.beans.AProject;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.Table;

@NoArgsConstructor
@Data
@Inheritance
public class ResearchFundingProject extends AProject {

    private Boolean isSmallProject;

    private EStateType state;

}
