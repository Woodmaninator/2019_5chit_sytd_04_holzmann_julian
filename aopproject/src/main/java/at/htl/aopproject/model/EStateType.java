package at.htl.aopproject.model;

public enum EStateType{
    CREATED, APPROVED, IN_EXECUTION
}
