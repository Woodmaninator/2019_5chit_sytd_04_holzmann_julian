package at.htl.aopproject.model.beans;

import at.htl.aopproject.model.EStateType;
import at.htl.aopproject.model.RequestFundingProject;
import at.htl.aopproject.model.ResearchFundingProject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectFactory {

    @RequestFundingProjectA
    @Bean("requestFundingProject")
    public at.htl.aopproject.model.beans.AProject createRequestFundingProject() {
        RequestFundingProject project = new RequestFundingProject();

        project.setTitle("Default Project");
        project.setIsFFGProject(true);

        return project;
    }

    @ResearchFundingProjectA
    @Bean("researchFundingProject")
    public at.htl.aopproject.model.beans.AProject createResearchFundingProject() {
        ResearchFundingProject project = new ResearchFundingProject();

        project.setIsSmallProject(true);
        project.setState(EStateType.APPROVED);

        return project;
    }

}
