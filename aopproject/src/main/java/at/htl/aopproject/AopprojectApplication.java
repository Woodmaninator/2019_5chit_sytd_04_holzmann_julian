package at.htl.aopproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AopprojectApplication.class, args);
    }

}
