package at.htl.aopproject.unit;

import at.htl.aopproject.model.beans.AProject;
import at.htl.aopproject.model.beans.RequestFundingProjectA;
import at.htl.aopproject.model.beans.ResearchFundingProjectA;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AOPUnitTest {
    @RequestFundingProjectA()
    @Autowired
    private AProject requestProject1;

    @RequestFundingProjectA()
    @Autowired
    private AProject requestProject2;

    @ResearchFundingProjectA()
    @Autowired
    private AProject researchProject;


    @Test
    public void testAOPLogic() {
        assertEquals(requestProject1, requestProject2);
        assertNotEquals(requestProject1, researchProject);
    }

}
