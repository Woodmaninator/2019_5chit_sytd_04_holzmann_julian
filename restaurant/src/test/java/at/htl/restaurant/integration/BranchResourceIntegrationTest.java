package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.model.ABranch;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.swing.text.html.parser.Entity;
import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class BranchResourceIntegrationTest {
    private static Logger log = LoggerFactory.getLogger(BranchResourceIntegrationTest.class);

    @Autowired
    private IABranchRepository branchRepository;

    @Test
    public void greetingTest(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches/greet");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<String> httpResponse = restClient.getForEntity(requestURL, String.class);

        String msg = httpResponse.getBody();

        assertEquals(HttpStatus.OK,httpResponse.getStatusCode());
        assertEquals("Hello World",msg);
    }

    @Transactional
    @Test
    public void getBranchByIdTest(){
        int id = 1;
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","branches",id);

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<ABranch> httpResponse = restClient.getForEntity(requestURL, ABranch.class);

        ABranch expected = branchRepository.getOne((long)id);
        ABranch actual = httpResponse.getBody();

        assertEquals(HttpStatus.OK,httpResponse.getStatusCode());
        assertEquals(expected,actual);
    }

    @Transactional
    @Test
    public void createBranch(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches");
        RestTemplate restClient = new RestTemplate();

        ABranch branch = EntityFactory.createDefaultBranch();
        HttpEntity<ABranch> requestData = new HttpEntity<>(branch);

        ResponseEntity<ABranch> httpResponse = restClient.postForEntity(requestURL, requestData, ABranch.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        ABranch createdBranch = httpResponse.getBody();

        assertNotNull(createdBranch.getId());
    }

    @Transactional
    @Test
    public void updateBranch(){
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","branches",1);

        ABranch branch = (ABranch) Hibernate.unproxy(branchRepository.getOne(1l));
        branch.setDistrict("Krems a. d. Donau");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<ABranch> requestData = new HttpEntity<>(branch, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT, requestData, Void.class);
        ABranch updatedBranch = branchRepository.getOne(1l);

        assertEquals("Krems a. d. Donau", updatedBranch.getDistrict());
    }
    @Transactional
    @Test
    public void deleteBranch(){
        ABranch branch = EntityFactory.createDefaultBranch();
        branch = branchRepository.save(branch);

        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","branches",branch.getId());
        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<ABranch> delBranch = branchRepository.findById(branch.getId());
        assertFalse(delBranch.isPresent());
    }
}