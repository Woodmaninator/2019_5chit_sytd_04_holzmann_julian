package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IIngredientRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class DishResourceIntegrationTest {
    private static Logger log = LoggerFactory.getLogger(BranchResourceIntegrationTest.class);

    @Autowired
    private IDishRepository dishRepository;

    @Autowired
    private IIngredientRepository ingredientRepository;

    @Transactional
    @Test
    public void getDishByIdTest(){
        int id = 1;
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","dishes",id);

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Dish> httpResponse = restClient.getForEntity(requestURL, Dish.class);

        Dish expected = dishRepository.getOne((long)id);
        Dish actual = httpResponse.getBody();

        assertEquals(HttpStatus.OK,httpResponse.getStatusCode());
        assertEquals(expected,actual);
    }

    @Transactional
    @Test
    public void createDishTest(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","dishes");
        RestTemplate restClient = new RestTemplate();

        Dish branch = EntityFactory.createDefaultDish();
        HttpEntity<Dish> requestData = new HttpEntity<Dish>(branch);

        ResponseEntity<Dish> httpResponse = restClient.postForEntity(requestURL, requestData, Dish.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        Dish createdBranch = httpResponse.getBody();

        assertNotNull(createdBranch.getId());
    }

    @Transactional
    @Test
    public void updateDishTest(){
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","dishes",1);

        Dish dish = (Dish) Hibernate.unproxy(dishRepository.getOne(1l));
        dish.setPrice(500);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Dish> requestData = new HttpEntity<Dish>(dish, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT, requestData, Void.class);
        Dish updatedDish = dishRepository.getOne(1l);

        assertEquals(500, (int)updatedDish.getPrice());
    }
    @Transactional
    @Test
    public void deleteDishTest(){
        Dish dish = EntityFactory.createDefaultDish();
        dish = dishRepository.save(dish);

        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","dishes",dish.getId());
        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);
        dishRepository.flush();


        Optional<Dish> delDish = dishRepository.findById(dish.getId());
        assertFalse(delDish.isPresent());
    }
    @Transactional
    @Test
    public void getDishesByIngredientTest(){
        String requestURL = String.format("%s/%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","dishes","dishesByIngredient",1l);
        List<Dish> expectedDishes = dishRepository.getDishesByIngredient(ingredientRepository.getOne(1l));

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<List> httpResponse = restClient.getForEntity(requestURL, List.class);

        List<Dish> actualDishes = (List<Dish>) httpResponse.getBody();

        assertEquals(HttpStatus.OK,httpResponse.getStatusCode());
        assertEquals(expectedDishes.size(),actualDishes.size());
    }
    @Transactional
    @Test
    public void renameDishTest(){
        String requestURL = String.format("%s/%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","dishes","rename",1l);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<String> requestData = new HttpEntity<String>("asdf", httpHeaders);
        RestTemplate restClient = new RestTemplate();
        restClient.exchange(requestURL, HttpMethod.PUT, requestData, Void.class);

        String expected = "asdf";
        String actual = ((Dish) Hibernate.unproxy(dishRepository.getOne(1l))).getName();

        assertEquals(expected, actual);
    }
}
