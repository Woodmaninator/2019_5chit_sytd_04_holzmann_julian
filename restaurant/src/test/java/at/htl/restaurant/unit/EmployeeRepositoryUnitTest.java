package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeRepositoryUnitTest {
    private static Logger log = LoggerFactory.getLogger(EmployeeRepositoryUnitTest.class);

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Autowired
    private IDishRepository dishRepository;

    @Transactional
    @Test
    public void readEmpleyeeById()
    {
        Employee employee = employeeRepository.readEmployeeById(1l);
        assertEquals(new Long(1l),employee.getId());
    }
    @Transactional
    @Test
    public void readEmployeeBySocialSecurityNumber()
    {
        Employee employee = employeeRepository.readEmployeeBySocialSecurityNumber("17091999");
        assertEquals(employee.getLastName(),"Draeger");
    }
    @Transactional
    @Test
    public void readEmployessByLastName(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameOrderByLastNameAsc("Leister");
        assertEquals(1,employees.size());
    }
    @Transactional
    @Test
    public void readEmployeesByLastNameToken(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameContains("K");
        assertEquals(3,employees.size());
    }
    @Transactional
    @Test
    public void readCooks(){
        List<Cook> cooks = employeeRepository.readCooksOrderByLastNameAsc();
        assertEquals(7,cooks.size());
    }
    @Transactional
    @Test
    public void readWaitersByBranch(){
        List<Waiter> waiters = employeeRepository.getWaitersByBranchID(1l);
        assertEquals(3,waiters.size());
    }
    @Transactional
    @Test
    public void readCooksByDish(){
        Dish dish = dishRepository.getOne(1l);
        List<Cook> cooks = employeeRepository.getCooksByDish(dish);
        assertEquals(2,cooks.size());
    }
    @Transactional
    @Test
    public void readTaablesByWaiter(){
        Waiter waiter = employeeRepository.getWaitersByBranchID(1l).get(0);
        List<Taable> taables = employeeRepository.getTablesByWaiter(waiter);

        assertEquals(4,taables.size());
    }
}
