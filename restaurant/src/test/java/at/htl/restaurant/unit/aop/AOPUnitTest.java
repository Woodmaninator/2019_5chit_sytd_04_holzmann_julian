package at.htl.restaurant.unit.aop;

import at.htl.restaurant.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class AOPUnitTest {
    private static Logger log = LoggerFactory.getLogger(AOPUnitTest.class);

    @Autowired
    @Qualifier("Cook")
    private Employee cook1;

    @Autowired
    @Qualifier("Cook")
    private Employee cook2;

    @Autowired
    @Qualifier("Waiter")
    private Employee waiter;

    @Test
    public void testAOPLogic() {
        assertEquals(cook1.getLastName(), cook2.getLastName());
        assertNotEquals(cook1.getSocialSecurityNumber(), cook2.getSocialSecurityNumber());
        assertNotEquals(cook1.getSocialSecurityNumber(), waiter.getSocialSecurityNumber());
    }
}
