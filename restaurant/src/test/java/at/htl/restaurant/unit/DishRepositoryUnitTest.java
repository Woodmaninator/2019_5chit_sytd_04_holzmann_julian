package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IIngredientRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Ingredient;
import at.htl.restaurant.model.Taable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DishRepositoryUnitTest {
    private static Logger log = LoggerFactory.getLogger(EmployeeRepositoryUnitTest.class);

    @Autowired
    private IABranchRepository branchRepository;

    @Autowired
    private IDishRepository dishRepository;

    @Autowired
    private IIngredientRepository ingredientRepository;

    @Transactional
    @Test
    public void getDishesByIngredient(){
        Ingredient ingredient = ingredientRepository.getOne(1l);
        List<Dish> dishes = dishRepository.getDishesByIngredient(ingredient);
        assertEquals(4,dishes.size());
    }
    @Transactional
    @Test
    public void getOrderCountByDish(){
        Dish dish = dishRepository.getOne(1l);
        int orderCount = dishRepository.getCountOrders(dish);

        assertEquals(1,orderCount);
    }
    @Transactional
    @Test
    public void getBranchesByDish(){
        Dish dish = dishRepository.getOne(1l);
        List<ABranch> branches = dishRepository.getBranchesByDish(dish);

        assertEquals(1,branches.size());
    }
    @Transactional
    @Test
    public void getHighestValuedTaableByBranch(){
        ABranch branch = branchRepository.getOne(1l);
        List<Taable> taables = dishRepository.getHighestValuedTaableByBranch(branch);
        long expected = 2;
        assertEquals(expected, (long)taables.get(0).getId());
    }
    @Transactional
    @Test
    public void getMaxOrderedDish(){
        List<Dish> dishes = dishRepository.getMaxOrderedDish();
        long expected = 11;
        assertEquals(expected, (long)dishes.get(0).getId());
    }
}
