package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.BranchGarden;
import at.htl.restaurant.model.Taable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BranchRepositoryUnitTest {
    private static Logger log = LoggerFactory.getLogger(EmployeeRepositoryUnitTest.class);

    @Autowired
    private IABranchRepository aBranchRepository;

    @Transactional
    @Test
    public void getTableCountByBranch(){
        ABranch branch = aBranchRepository.getOne(1l);
        int tableCount = aBranchRepository.getTableCountByBranch(branch);
        assertEquals(4, tableCount);
    }
    @Transactional
    @Test
    public void getBranchesByToken(){
        List<ABranch> branches = aBranchRepository.readABranchesByNameContains("m");
        assertEquals(5,branches.size());
    }
}
