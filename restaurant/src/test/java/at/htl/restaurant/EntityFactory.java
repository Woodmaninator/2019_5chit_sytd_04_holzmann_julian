package at.htl.restaurant;

import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.DishType;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class EntityFactory {
    public static Branch createDefaultBranch(){
        Branch branch = new Branch();

        branch.setName("Pizza Friska");
        branch.setDistrict("Tulln");
        branch.setAddress("Kirchberg am Wagram");

        SimpleDateFormat df = new SimpleDateFormat("hh:mm");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        return branch;
    }
    public static Dish createDefaultDish(){
        Dish dish = new Dish();

        dish.setName("Krautsalat");
        dish.setPrice(1000);
        dish.setType(DishType.ZUSPEISE);
        dish.setVersion(1l);

        return dish;
    }
}
