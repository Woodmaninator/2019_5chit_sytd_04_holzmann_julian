package at.htl.restaurant.domain;

import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Ingredient;
import at.htl.restaurant.model.Taable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IDishRepository extends JpaRepository<Dish,Long> {
    @Query("SELECT x.dishIngredientID.dish FROM DishIngredient x WHERE x.dishIngredientID.ingredient = :ingredientInput")
    List<Dish> getDishesByIngredient(@Param("ingredientInput")Ingredient ingredient);

    @Query("SELECT count(x.orderTaableDishID.order) FROM OrderTaableDish x where x.orderTaableDishID.dish = :dishInput")
    int getCountOrders(@Param("dishInput") Dish dish);

    @Query("SELECT y.branchEmployeeID.branch " +
            "FROM CookDish x " +
            "JOIN BranchEmployee y " +
            "ON x.cookDishID.cook = y.branchEmployeeID.employee " +
            "WHERE x.cookDishID.dish = :inputDish " +
            "ORDER BY y.branchEmployeeID.branch.name")
    List<ABranch> getBranchesByDish(@Param("inputDish")Dish dish);

    @Query("SELECT x.orderTaableDishID.taable " +
            "FROM OrderTaableDish x " +
            "WHERE x.orderTaableDishID.taable.branch = :branchInput " +
            "GROUP BY x.orderTaableDishID.taable.id " +
            "HAVING SUM(x.orderTaableDishID.dish.price) >= ALL " +
            "(SELECT SUM(y.orderTaableDishID.dish.price) " +
            "FROM OrderTaableDish y " +
            "WHERE y.orderTaableDishID.taable.branch = :branchInput " +
            "GROUP BY y.orderTaableDishID.taable.id)")
    List<Taable> getHighestValuedTaableByBranch(@Param("branchInput")ABranch branch);

    @Query("SELECT x.orderTaableDishID.dish " +
            "FROM OrderTaableDish x " +
            "GROUP BY x.orderTaableDishID.dish.id " +
            "HAVING COUNT(x.orderTaableDishID.dish.id) >= ALL " +
            "(SELECT COUNT(x.orderTaableDishID.dish.id) " +
            "FROM OrderTaableDish y " +
            "GROUP BY y.orderTaableDishID.dish.id)")
    public List<Dish> getMaxOrderedDish();
}
