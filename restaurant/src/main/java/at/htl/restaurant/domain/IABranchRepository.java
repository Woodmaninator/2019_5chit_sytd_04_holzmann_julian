package at.htl.restaurant.domain;

import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.BranchGarden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IABranchRepository extends JpaRepository<ABranch,Long> {
    @Query("SELECT count(t.id) FROM Taable t JOIN ABranch b ON b = t.branch WHERE b = :branchInput")
    int getTableCountByBranch(@Param("branchInput") ABranch branch);

    List<ABranch> readABranchesByNameContains(String token);
}
