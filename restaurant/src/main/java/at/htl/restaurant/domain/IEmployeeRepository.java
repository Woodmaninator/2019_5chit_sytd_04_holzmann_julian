package at.htl.restaurant.domain;

import at.htl.restaurant.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IEmployeeRepository extends JpaRepository<Employee,Long> {
    Employee readEmployeeById(Long id);

    Employee readEmployeeBySocialSecurityNumber(String socialSecurityNumber);

    List<Employee> readEmployeesByLastNameOrderByLastNameAsc(String lastName);

    List<Employee> readEmployeesByLastNameContains(String token);

    @Query("SELECT c FROM Employee c WHERE type(c) = Cook ORDER BY c.lastName")
    List<Cook> readCooksOrderByLastNameAsc();

    @Query("SELECT x.branchEmployeeID.employee FROM BranchEmployee x join x.branchEmployeeID.employee e WHERE x.branchEmployeeID.branch.id = :branchId AND type(e) = Waiter")
    List<Waiter> getWaitersByBranchID(@Param("branchId") Long id);

    @Query("SELECT x.cookDishID.cook FROM CookDish x WHERE x.cookDishID.dish = :dishInput")
    List<Cook> getCooksByDish(@Param("dishInput") Dish dish);

    @Query("SELECT x.waiterTaableID.taable FROM WaiterTaable x WHERE x.waiterTaableID.waiter = :waiterInput")
    List<Taable> getTablesByWaiter(@Param("waiterInput") Waiter waiter);
}
