package at.htl.restaurant.service.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class EntityNotFoundAdvice {
    @ExceptionHandler(EntityNotFoundException.class) //für welche Exception die untere Methode aufgerufen wird
    //Voraussetzung ist, dass bei der Methode throws EntityNotFoundException am Ende steht
    //überall wo throws EntityNotFoundException geworfen wird, wird beim Auftritt der Exception auf die untere Methode verwiesen und die
    //Statuscodes und Response Bodies werden dementsprechend bei der Antwort gesetzt
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String entityNotFoundException(EntityNotFoundException ex) {
        return ex.getMessage();
    }
}
