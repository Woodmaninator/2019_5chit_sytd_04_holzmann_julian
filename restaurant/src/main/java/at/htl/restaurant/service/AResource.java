package at.htl.restaurant.service;

import at.htl.restaurant.model.AEntity;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.Serializable;

public abstract class AResource<T extends AEntity, id extends Serializable> {

    public AResource(JpaRepository<T,id> repository){
        this.repository = repository;
    }

    private static Logger log = LoggerFactory.getLogger(AResource.class);

    private JpaRepository<T, id> repository;

    @Transactional
    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public T read(@PathVariable("id") id id){
        T resource = repository.getOne(id);
        log.info("loaded branch with id: " + resource.toString());
        return (T) Hibernate.unproxy(resource);
    }

    @Transactional
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public T create(@RequestBody @Valid T resource){
        T createResource = repository.save(resource);
        log.info("created resource " + createResource.getId());
        return (T) Hibernate.unproxy(createResource);
    }

    @Transactional
    @PutMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody T resource, @PathVariable("id") id id){
        repository.save(resource);
        log.info("Updated ID: " + resource.getId());
    }

    @Transactional
    @DeleteMapping(path="/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") id id){

        repository.delete(repository.getOne(id));
        log.info("branch removed id: " + id);
    }
}
