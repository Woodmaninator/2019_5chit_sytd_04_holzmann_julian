package at.htl.restaurant.service;

import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IIngredientRepository;
import at.htl.restaurant.model.Dish;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RequestMapping(path="/dishes")
@RestController
public class DishResource extends AResource<Dish, Long> {

    @Autowired
    private IDishRepository dishRepository;
    @Autowired
    private IIngredientRepository ingredientRepository;

    public DishResource(@Autowired JpaRepository<Dish, Long> repository) {
        super(repository);
    }

    @GetMapping(path="/dishesByIngredient/{ingredientId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public List<Dish> readDishesByIngredient(@PathVariable Long ingredientId){
        List<Dish> dishList = dishRepository.getDishesByIngredient(
                ingredientRepository.getOne(ingredientId));
        return (List<Dish>) Hibernate.unproxy(dishList);
    }
    @Transactional
    @PutMapping(path="/rename/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void rename(@PathVariable("id") Long id, @RequestBody String name){
        Dish dish = (Dish) Hibernate.unproxy(dishRepository.getOne(id));
        dish.setName(name);
        dishRepository.save(dish);
    }
}
