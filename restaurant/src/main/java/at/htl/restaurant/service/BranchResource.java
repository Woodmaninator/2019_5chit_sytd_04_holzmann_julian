package at.htl.restaurant.service;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.model.ABranch;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RequestMapping(path="/branches")
@RestController
public class BranchResource {

    private static Logger log = LoggerFactory.getLogger(BranchResource.class);

    @Autowired
    private IABranchRepository branchRepository;

    @GetMapping(path="/greet", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String greeting(){
        log.info("greeting was issued");
        return "Hello World";
    }

    @Transactional
    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ABranch read(@PathVariable("id") Long id){
        ABranch branch = branchRepository.getOne(id);
        log.info("loaded branch with id: " + branch.toString());
        return (ABranch) Hibernate.unproxy(branch);
    }

    @Transactional
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ABranch create(@RequestBody ABranch branch){
        ABranch createResource = branchRepository.save(branch);
        log.info("created resource " + createResource.getId());
        return (ABranch) Hibernate.unproxy(createResource);
    }

    @Transactional
    @PutMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody ABranch branch, @PathVariable("id") Long id){
        branchRepository.save(branch);
        log.info("Updated ID: " + branch.getId());
    }

    @Transactional
    @DeleteMapping(path="/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id){
        branchRepository.deleteById(id);
        log.info("branch removed id: " + id);
    }
}
