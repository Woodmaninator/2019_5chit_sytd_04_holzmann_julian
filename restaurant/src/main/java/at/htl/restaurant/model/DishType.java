package at.htl.restaurant.model;

public enum DishType {
    VORSPEISE, HAUPTSPEISE, ZUSPEISE, NACHSPEISE
}
