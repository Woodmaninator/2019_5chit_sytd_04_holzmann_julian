package at.htl.restaurant.model;


import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Table(name="TISCHE")
@Entity
@Data
public class Taable extends AEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name="FILIALE_ID",nullable = false)
    private ABranch branch;


    @Column(name ="VERSION")
    private Long version;

    @NotNull
    @Column(name = "TISCH_NR",nullable = false)
    private Integer tableNumber;

    @NotNull
    @Column(name="IST_RAUCHER_TISCH",nullable = false)
    private Boolean isSmokingTable;


}
