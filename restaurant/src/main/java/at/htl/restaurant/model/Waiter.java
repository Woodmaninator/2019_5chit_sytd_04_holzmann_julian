package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name="KELLNER")
@Entity
@Data
public class Waiter extends Employee {
}
