package at.htl.restaurant.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="GERICHTE")
@Data
public class Dish extends AEntity{
    @Column(name="VERSION")
    private Long version;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(name="ART",length=255,nullable = false)
    private DishType type;

    @Length(max=50)
    @NotNull
    @Column(name="BEZEICHNUNG",length=50,nullable = false)
    private String name;

    @NotNull
    @Column(name="PREIS",nullable = false)
    private Integer price;
}
