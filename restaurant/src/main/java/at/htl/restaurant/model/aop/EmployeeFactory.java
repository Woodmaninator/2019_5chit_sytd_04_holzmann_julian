package at.htl.restaurant.model.aop;

import at.htl.restaurant.model.Cook;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class EmployeeFactory {

    @Autowired
    private IDGenerator generator;

    @Bean
    @Qualifier("Cook")
    @Scope("prototype")
    public Employee createDefaultCook() {
        Cook cook = new Cook();

        cook.setLastName("Nagelmaier");
        cook.setFirstName("Jonas");
        cook.setSocialSecurityNumber(generator.generateSocialSecurityCode());

        return cook;
    }

    @Bean
    @Qualifier("Waiter")
    @Scope("prototype")
    public Employee createDefaultWaiter() {
        Waiter waiter = new Waiter();

        waiter.setLastName("Schandl");
        waiter.setFirstName("Lukas");
        waiter.setSocialSecurityNumber(generator.generateSocialSecurityCode());

        return waiter;
    }

}
