package at.htl.restaurant.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Table(name="SESSEL")
@Entity
@Data
public class Chair extends AEntity {

    @NotNull
    @Length(max=3)
    @Column(name = "CODE",length=3,unique = true,nullable = false)
    private String code;

    @Column(name="VERSION")
    private Long version;

    @ManyToOne
    @JoinColumn(name="TABLE_ID")
    private Taable table;
}
