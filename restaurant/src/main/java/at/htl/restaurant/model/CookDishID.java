package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Embeddable
@Data
public class CookDishID implements Serializable {
    @ManyToOne
    @JoinColumn(name="COOK_ID")
    private Cook cook;
    @ManyToOne
    @JoinColumn(name="DISH_LIST_ID")
    private Dish dish;
}
