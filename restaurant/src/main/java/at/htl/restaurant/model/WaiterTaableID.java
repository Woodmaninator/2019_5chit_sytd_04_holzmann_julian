package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
public class WaiterTaableID implements Serializable {

    @ManyToOne
    @JoinColumn(name="WAITER_ID")
    private Waiter waiter;

    @ManyToOne
    @JoinColumn(name="TABLE_LIST_ID")
    private Taable taable;
}
