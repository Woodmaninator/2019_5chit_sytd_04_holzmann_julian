package at.htl.restaurant.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = JsonTypeInfo.As.PROPERTY,property="json-type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Waiter.class, name = "waiter"),
        @JsonSubTypes.Type(value = Cook.class, name = "cook")
})
@Entity
@Table(name="ANGESTELLTE")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class Employee extends AEntity {

    @Column(name="VERSION")
    private Long version;

    @NotNull
    @Length(max=50)
    @Column(name="VORNAME",nullable = false,length = 50)
    private String firstName;

    @Length(max=50)
    @NotNull
    @Column(name="NACHNAME",nullable = false,length = 50)
    private String lastName;

    @NotNull
    @Length(max=10)
    @Column(name="SOZIALVERSICHERUNGS_NR",nullable = false, unique = true,length=10)
    private String socialSecurityNumber;
}
