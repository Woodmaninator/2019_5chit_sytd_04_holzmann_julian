package at.htl.restaurant.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ZUTATEN")
@Data
public class Ingredient extends AEntity{

    @Column(name="VERSION")
    private Long version;

    @Length(max=50)
    @NotNull
    @Column(name="BEZEICHNUNG",length=50,nullable = false,unique = true)
    private String name;

    @NotNull
    @Column(name="LAGERBESTAND",nullable = false)
    private Integer stock;
}
