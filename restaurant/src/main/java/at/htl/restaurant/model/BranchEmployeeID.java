package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Embeddable
@Data
public class BranchEmployeeID implements Serializable {

    @ManyToOne
    @JoinColumn(name="BRANCH_LIST_ID")
    private ABranch branch;

    @ManyToOne
    @JoinColumn(name="AEMPLOYEE_ID")
    private Employee employee;
}
