package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="GERICHTE_ZUTATEN")
@Data
public class DishIngredient implements Serializable {

    @EmbeddedId
    private DishIngredientID dishIngredientID;

    @Column(name="VERSION")
    private Long version;

    @NotNull
    @Column(name="MENGE",nullable = false)
    private Integer amount;
}
