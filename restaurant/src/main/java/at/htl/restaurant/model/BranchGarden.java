package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="FILIALEN_GASTGARTEN")
@Data
public class BranchGarden extends ABranch {

    @Temporal(TemporalType.TIME)
    @Column(name="OEFFNUNGS_ZEIT",nullable = false)
    private Date openingTime;

    @Temporal(TemporalType.TIME)
    @Column(name="SCHLUSS_ZEIT",nullable = false)
    private Date closingTime;
}
