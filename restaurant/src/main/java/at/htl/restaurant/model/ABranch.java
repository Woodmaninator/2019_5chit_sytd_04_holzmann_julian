package at.htl.restaurant.model;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,include = JsonTypeInfo.As.PROPERTY,property="json-type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Branch.class, name = "branch"),
        @JsonSubTypes.Type(value = BranchGarden.class, name = "branchGarden")
})
@Entity
@Table(name="A_FILIALEN")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public abstract class ABranch extends AEntity {


    @Column(name="VERSION")
    private long version;

    @Length(max=30)
    @NotNull
    @Column(name="BEZIRK",length = 30,nullable = false)
    private String district;

    @NotNull
    @Length(max=200)
    @Column(name="ADRESSE",length = 200,nullable = false)
    private String address;

    @Length(max=50)
    @NotNull
    @Column(name="NAME",length = 50,unique = true, nullable = false)
    private String name;
}
