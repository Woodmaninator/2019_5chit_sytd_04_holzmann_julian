package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="FILIALEN_ANGESTELLTE")
@Entity
@Data
public class BranchEmployee implements Serializable {

    @EmbeddedId
    private BranchEmployeeID branchEmployeeID;
}
