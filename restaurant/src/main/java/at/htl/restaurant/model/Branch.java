package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="FILIALEN")
@Data
public class Branch extends ABranch {
}
