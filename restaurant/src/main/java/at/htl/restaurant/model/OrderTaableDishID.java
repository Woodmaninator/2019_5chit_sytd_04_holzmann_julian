package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
public class OrderTaableDishID implements Serializable {

    @ManyToOne
    @JoinColumn(name="BESTELLUNGS_ID")
    private Order order;
    @ManyToOne
    @JoinColumn(name="TISCH_ID")
    private Taable taable;
    @ManyToOne
    @JoinColumn(name="GERICHT_ID")
    private Dish dish;
}
