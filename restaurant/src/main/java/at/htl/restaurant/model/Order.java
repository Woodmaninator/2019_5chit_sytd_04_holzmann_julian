package at.htl.restaurant.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name="BESTELLUNGEN")
@Data
public class Order extends AEntity {

    @Column(name="VERSION")
    private Long version;

    @Temporal(TemporalType.TIME)
    @NotNull
    @Column(name="BESTELLUNGS_ZEITPUNKT",nullable = false)
    private Date orderTime;

    @NotNull
    @Length(max=255)
    @Column(name="VERLAUF",nullable = false)
    private String history;

    @NotNull
    @Column(name="BESTELLUNGS_ID",nullable = false)
    private Integer orderId;
}
