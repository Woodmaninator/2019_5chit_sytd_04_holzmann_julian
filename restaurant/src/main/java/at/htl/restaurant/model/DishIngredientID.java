package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
public class DishIngredientID implements Serializable {

    @ManyToOne
    @JoinColumn(name="GERICHT_ID")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name="ZUTAT_ID")
    private Ingredient ingredient;
}
