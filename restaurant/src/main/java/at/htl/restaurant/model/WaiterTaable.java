package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="KELLNER_TISCHE")
@Entity
@Data
public class WaiterTaable implements Serializable {

    @EmbeddedId
    private WaiterTaableID waiterTaableID;
}
