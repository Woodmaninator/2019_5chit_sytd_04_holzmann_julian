package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="BESTELLUNGEN_TISCHE_GERICHTE")
@Data
public class OrderTaableDish implements Serializable {
    @EmbeddedId
    private OrderTaableDishID orderTaableDishID;

    @Column(name="VERSION")
    private Long version;
}
