package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="KOECHE")
@Data
public class Cook extends Employee{
}
