package at.htl.restaurant.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RuntimeAspect {

    private static Logger log = LoggerFactory.getLogger(RuntimeAspect.class);

    @After("execution(* at.htl.restaurant.service.BranchResource.*(..))")
    public void logCall(JoinPoint jp) {
        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        log.info(String.format("method call: %s", methodSignature.toString()));
    }

    @Around("execution(* at.htl.restaurant.service.*Resource.*(..))")
    public Object execute(ProceedingJoinPoint jp) throws Throwable {
        Object result = null;
        long begin = System.currentTimeMillis();

        result = jp.proceed();

        long end = System.currentTimeMillis();
        log.info(String.format("runtime: %d ms", end - begin));

        return result;
    }
}