package at.htl.chat.demo.chat.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;


@Component
@Aspect
public class RuntimeAspect {

    private static Logger log = LoggerFactory.getLogger(RuntimeAspect.class);

    @Before("execution(* at.htl.chat.demo.chat.ChatServer.sendMessage(String))")
    public void logCall(JoinPoint jp) throws IOException {
        FileWriter fileWriter = new FileWriter("C:\\Users\\Julian\\Documents\\5CHIT\\SYTD\\chatlog.txt", true); //Set true for append mode
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("Received Message: " + jp.getArgs()[0].toString());  //New line
        printWriter.close();
    }
}

