package at.htl.chat.demo.chat;

import java.util.ArrayList;
import java.util.List;

public class ChatServer {
    private List<ChatClient> clients;

    public ChatServer(){
        clients = new ArrayList<>();
    }

    public void attach(ChatClient client){
        clients.add(client);
    }
    public void detach(ChatClient client){
        clients.remove(client);
    }
    public void sendMessage(String message){
        for (ChatClient client : clients){
            client.update(message);
        }
    }
}
