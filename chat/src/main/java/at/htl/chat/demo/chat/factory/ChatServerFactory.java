package at.htl.chat.demo.chat.factory;

import at.htl.chat.demo.chat.ChatServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChatServerFactory {

    @Bean()
    public ChatServer CreateChatServer(){
        ChatServer server = new ChatServer();
        return server;
    }
}
