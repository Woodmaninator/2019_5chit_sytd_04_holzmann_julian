package at.htl.chat.demo.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatClient {

    private Logger log = LoggerFactory.getLogger(ChatClient.class);

    private ChatServer server;

    public ChatClient(ChatServer server){
        this.server = server;
        this.server.attach(this);
    }

    public void sendMessage(String message) {
        this.server.sendMessage(message);
    }

    public void update(String message){
        log.info("Message received: " + message);
    }
}
