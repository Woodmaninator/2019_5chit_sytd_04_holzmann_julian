package at.htl.chat.demo.unit;

import at.htl.chat.demo.chat.ChatClient;
import at.htl.chat.demo.chat.ChatServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ChatUnitTest {

    @Autowired
    private ChatServer server;

    @Test
    public void TestChatFunctionality(){
        ChatClient client1 = new ChatClient(this.server);
        ChatClient client2 = new ChatClient(this.server);

        client1.sendMessage("Test Message from Client 1");
        client2.sendMessage("Test Message from Client 2");
    }
}
