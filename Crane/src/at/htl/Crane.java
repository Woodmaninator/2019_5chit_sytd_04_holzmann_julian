package at.htl;

import java.util.concurrent.Semaphore;

public class Crane implements Runnable {
    private Semaphore semA;
    private Semaphore semB;
    private Semaphore semC;

    public Crane(Semaphore semA, Semaphore semB, Semaphore semC){
        this.semA = semA;
        this.semB = semB;
        this.semC = semC;
    }

    @Override
    public void run() {
        try {
            while (true) {
                move1();
                semA.release();
                semC.acquire();
                move2();
                semB.release();
                semC.acquire();
                move3();
            }
        } catch(InterruptedException ex){
            System.out.println(ex.getMessage());
        }
    }
    private void move1(){
        System.out.println("Moving from Warehouse 1 to Machine A");
    }
    private void move2(){
        System.out.println("Moving from Machine A to Machine B");
    }
    private void move3(){
        System.out.println("Moving from Machine B to Warehouse 2");
    }
}
