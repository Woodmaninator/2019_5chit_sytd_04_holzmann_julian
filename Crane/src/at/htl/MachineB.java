package at.htl;

import java.util.concurrent.Semaphore;

public class MachineB implements Runnable {
    private Semaphore semB;
    private Semaphore semC;

    public MachineB(Semaphore semB, Semaphore semC){
        this.semB = semB;
        this.semC = semC;
    }

    @Override
    public void run() {
        try{
            while(true){
                semB.acquire();
                process();
                semC.release();
            }
        } catch(InterruptedException ex){
            System.out.println(ex.getMessage());
        }
    }
    private void process(){
        System.out.println("Machine B is processing!");
    }
}
