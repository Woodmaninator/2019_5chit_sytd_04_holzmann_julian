package at.htl;

import java.util.concurrent.Semaphore;

public class MachineA implements Runnable {
    private Semaphore semA;
    private Semaphore semC;

    public MachineA(Semaphore semA, Semaphore semC){
        this.semA = semA;
        this.semC = semC;
    }

    @Override
    public void run() {
        try{
            while(true){
                semA.acquire();
                process();
                semC.release();
            }
        } catch(InterruptedException ex){
            System.out.println(ex.getMessage());
        }
    }
    private void process(){
        System.out.println("Machine A is processing!");
    }
}
