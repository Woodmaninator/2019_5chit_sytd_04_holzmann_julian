package at.htl;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
	// write your code here
        Semaphore semA = new Semaphore(0);
        Semaphore semB = new Semaphore(0);
        Semaphore semC = new Semaphore(0);

        Thread crane = new Thread(new Crane(semA,semB,semC));
        Thread machineA = new Thread(new MachineA(semA,semC));
        Thread machineB = new Thread(new MachineB(semB,semC));

        crane.start();
        machineA.start();
        machineB.start();
    }
}
