package at.htl.calculator.unit;

import at.htl.calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CalculatorUnitTest {

    @Autowired
    private Calculator calculator;

    @Test
    public void addNumberTest(){
        double expected = 7;
        double actual = calculator.add(2.5, 4.5);
        assertEquals(expected, actual, 0.1);
    }
    @Test
    public void subtractNumberTest(){
        double expected = 7;
        double actual = calculator.subtract(12, 5);
        assertEquals(expected, actual, 0.1);
    }
    @Test
    public void multiplyNumberTest(){
        double expected = 21;
        double actual = calculator.multiply(3, 7);
        assertEquals(expected, actual, 0.1);
    }
    @Test
    public void divideNumberTest(){
        double expected = 4;
        double actual = calculator.divide(8,2);
        assertEquals(expected, actual, 0.1);
    }
}
