package at.htl.calculator.factory;

import at.htl.calculator.Calculator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CalculatorFactory {
    @Bean
    public Calculator createCalculator(){
        return new Calculator();
    }
}
