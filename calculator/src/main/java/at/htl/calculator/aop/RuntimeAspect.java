package at.htl.calculator.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.Scanner;

@Component
@Aspect
public class RuntimeAspect {
    @AfterReturning(pointcut = "execution(double at.htl.calculator.Calculator.*(double, double))", returning = "retVal")
    public void AfterCalculatorExecution(JoinPoint jp, Object retVal) throws IOException, ParserConfigurationException, SAXException, TransformerException {
        Double input1 = (Double)jp.getArgs()[0];
        Double input2 = (Double)jp.getArgs()[1];
        Double result = (Double)retVal;

        //CREATING THE XML FILE
        String path = "C:\\Users\\Julian\\Documents\\5CHIT\\SYTD\\2019_5chit_sytd_04_holzmann_julian\\calculator\\calculations.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        StringBuilder xmlStringBuilder = new StringBuilder();
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        while(scanner.hasNextLine()){
            xmlStringBuilder.append(scanner.nextLine());
        }
        scanner.close();

        ByteArrayInputStream input = new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
        Document doc = builder.parse(input);

        Element root = doc.getDocumentElement();

        Node calculationNode = doc.createElement("Calculation");

        Node calculationTypeNode = doc.createElement("CalculationType");
        calculationTypeNode.setTextContent(jp.getSignature().toShortString());
        calculationNode.appendChild(calculationTypeNode);

        Node input1Node = doc.createElement("Input1");
        input1Node.setTextContent(input1.toString());
        calculationNode.appendChild(input1Node);

        Node input2Node = doc.createElement("Input2");
        input2Node.setTextContent(input2.toString());
        calculationNode.appendChild(input2Node);

        Node resultNode = doc.createElement("Result");
        resultNode.setTextContent(result.toString());
        calculationNode.appendChild(resultNode);

        root.appendChild(calculationNode);

        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

        tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(path)));
    }
}
