package at.htl;

public class HouseBlendCoffee implements ICoffeeBehaviour {
    @Override
    public String getDescreption() {
        return "House Blend Coffee \t";
    }

    @Override
    public double getPrice() {
        return 2.5;
    }
}
