package at.htl;

public interface ICoffeeBehaviour {
    String getDescreption();
    double getPrice();
}
