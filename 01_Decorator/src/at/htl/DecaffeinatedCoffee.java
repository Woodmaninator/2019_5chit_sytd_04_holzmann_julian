package at.htl;

public class DecaffeinatedCoffee implements ICoffeeBehaviour {
    @Override
    public String getDescreption() {
        return "Decaffeinated Coffee \t";
    }

    @Override
    public double getPrice() {
        return 1.5;
    }
}
