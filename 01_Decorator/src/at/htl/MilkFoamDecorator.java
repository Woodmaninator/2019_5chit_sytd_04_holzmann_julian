package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MilkFoamDecorator implements ICoffeeBehaviour{

    ICoffeeBehaviour component;

    @Override
    public String getDescreption() {
        return component.getDescreption() + "Milk Foam \t";
    }

    @Override
    public double getPrice() {
        return component.getPrice() + 0.75;
    }
}
