package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MilkDecorator implements ICoffeeBehaviour {

    ICoffeeBehaviour component;

    @Override
    public String getDescreption() {
        return component.getDescreption() + "Milk \t";
    }

    @Override
    public double getPrice() {
        return component.getPrice() + 0.5;
    }
}
