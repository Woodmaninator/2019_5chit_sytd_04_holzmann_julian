package at.htl;

public class EspressoCoffee implements ICoffeeBehaviour {
    @Override
    public String getDescreption() {
        return "Espresso \t";
    }

    @Override
    public double getPrice() {
        return 4;
    }
}
