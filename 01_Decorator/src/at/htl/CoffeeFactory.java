package at.htl;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CoffeeFactory {
    public ICoffeeBehaviour getCoffee(String input)
    {
        if(input == null)
            return null;
        if(input.equalsIgnoreCase("DEFAULTCOFFEE"))
        {
            return new MilkDecorator(new HouseBlendCoffee());
        }
        else if(input.equalsIgnoreCase("HARDCOREESPRESSO"))
        {
            return  new ChocolateDecorator(new EspressoCoffee());
        }
        else if(input.equalsIgnoreCase("DEFAULTWITHALL"))
        {
            return new ChocolateDecorator(new SoyDecorator(new MilkDecorator(new MilkDecorator(new HouseBlendCoffee()))));
        }
        else if(input.equalsIgnoreCase("DARKROASTWITHMILK"))
        {
            return new MilkDecorator(new DarkRoastCoffee());
        }
        else if(input.equalsIgnoreCase("DECAFFEINATEDCOFFEE"))
        {
            return new MilkDecorator(new MilkFoamDecorator(new DecaffeinatedCoffee()));
        }
        return null;
    }
}
