package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ChocolateDecorator implements ICoffeeBehaviour {

    ICoffeeBehaviour component;

    @Override
    public String getDescreption() {
        return component.getDescreption() + "Chocolate \t";
    }

    @Override
    public double getPrice() {
        return component.getPrice() + 2;
    }
}
