package at.htl;

import lombok.Data;
import lombok.Getter;

public class CoffeeBuilder {
    @Getter
    int amountMilk;
    @Getter
    int amountMilkFoam;
    @Getter
    int amountSoy;
    @Getter
    int amountChocolate;
    @Getter
    String coffeeType;

    public CoffeeBuilder setAmountMilk(int amountMilk)
    {
        this.amountMilk = amountMilk;
        return this;
    }
    public CoffeeBuilder setAmountMilkFoam(int amountMilkFoam)
    {
        this.amountMilkFoam = amountMilkFoam;
        return this;
    }
    public CoffeeBuilder setAmountSoy(int amountSoy)
    {
        this.amountSoy = amountSoy;
        return this;
    }
    public CoffeeBuilder setAmountChocolate(int amountChocolate)
    {
        this.amountChocolate = amountChocolate;
        return this;
    }
    public CoffeeBuilder setCoffeeType(String coffeeType)
    {
        this.coffeeType = coffeeType;
        return this;
    }

    public ICoffeeBehaviour build()
    {
        return new Coffee(this).getCoffee();
    }
}
