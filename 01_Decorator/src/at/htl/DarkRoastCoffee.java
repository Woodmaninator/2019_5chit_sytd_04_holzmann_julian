package at.htl;

public class DarkRoastCoffee implements ICoffeeBehaviour {
    @Override
    public String getDescreption() {
        return "Dark Roast Coffee \t";
    }

    @Override
    public double getPrice() {
        return 3;
    }
}
