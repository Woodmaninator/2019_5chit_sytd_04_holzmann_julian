package at.htl;

public class Main {

    public static void main(String[] args) {
	    ICoffeeBehaviour customCoffee1 = new MilkDecorator(new SoyDecorator(new DarkRoastCoffee()));
        System.out.println(customCoffee1.getDescreption());
        System.out.println(customCoffee1.getPrice());

        ICoffeeBehaviour customCoffee2 = new MilkDecorator(new ChocolateDecorator(new ChocolateDecorator(new DecaffeinatedCoffee())));
        System.out.println(customCoffee2.getDescreption());
        System.out.println(customCoffee2.getPrice());


        CoffeeBuilder builder = new CoffeeBuilder();
        ICoffeeBehaviour customCoffee3 = builder.setAmountChocolate(3).setCoffeeType("DARKROAST").build();
        System.out.println(customCoffee3.getDescreption());
        System.out.println(customCoffee3.getPrice());

        CoffeeFactory factory = new CoffeeFactory();
        ICoffeeBehaviour customCoffee4 = factory.getCoffee("DEFAULTCOFFEE");
        System.out.println(customCoffee4.getDescreption());
        System.out.println(customCoffee4.getPrice());
    }
}
