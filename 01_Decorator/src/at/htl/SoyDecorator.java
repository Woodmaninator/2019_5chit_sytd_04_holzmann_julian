package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SoyDecorator implements ICoffeeBehaviour {

    ICoffeeBehaviour component;

    @Override
    public String getDescreption() {
        return component.getDescreption() + "Soy \t";
    }

    @Override
    public double getPrice() {
        return component.getPrice() + 1.5;
    }
}
