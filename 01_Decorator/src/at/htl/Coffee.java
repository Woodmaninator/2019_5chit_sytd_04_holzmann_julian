package at.htl;

import lombok.Data;
import lombok.Getter;

@Data
public class Coffee{
    ICoffeeBehaviour coffee;
    public Coffee(CoffeeBuilder builder)
    {
        if(builder.getCoffeeType().equalsIgnoreCase("HOUSEBLEND"))
            coffee = new HouseBlendCoffee();
        else if(builder.getCoffeeType().equalsIgnoreCase("DARKROAST"))
            coffee = new DarkRoastCoffee();
        else if(builder.getCoffeeType().equalsIgnoreCase("ESPRESSO"))
            coffee = new EspressoCoffee();
        else if(builder.getCoffeeType().equalsIgnoreCase("DECAFFEINATEDCOFFEE"))
            coffee = new DecaffeinatedCoffee();
        else
            coffee = new HouseBlendCoffee();

        for(int i = 0; i < builder.getAmountChocolate(); i++)
            coffee = new ChocolateDecorator(coffee);
        for(int i = 0; i < builder.getAmountMilk(); i++)
            coffee = new MilkDecorator(coffee);
        for(int i = 0; i < builder.getAmountMilkFoam(); i++)
            coffee = new MilkFoamDecorator(coffee);
        for(int i = 0; i < builder.getAmountSoy(); i++)
            coffee = new SoyDecorator(coffee);
    }
}
