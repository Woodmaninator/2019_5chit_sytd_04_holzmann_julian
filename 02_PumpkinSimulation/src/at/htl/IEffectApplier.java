package at.htl;

import java.util.List;

public interface IEffectApplier {
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay);
}
