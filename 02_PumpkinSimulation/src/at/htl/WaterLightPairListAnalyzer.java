package at.htl;

import javafx.scene.effect.Light;

import java.util.List;

public class WaterLightPairListAnalyzer {
    public int getRecentWaterLevel(int currentDay, int length, List<LightWaterPair> list)
    {
        int waterSum = 0;
        if(currentDay >= length -1)
        {
            for(int i = 0; i < length; i++)
                waterSum += list.get(currentDay - i).getWater();
        }
        return waterSum;
    }
}
