package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ThirdEffectDecorator implements IPumpkinBehaviour {

    IPumpkinBehaviour component;

    @Override
    public int getGrowth(int light, int water) {
        return component.getGrowth(light, water) * 0;
    }
    @Override
    public String getDescription()
    {
        return component.getDescription() + "E3, ";
    }
}
