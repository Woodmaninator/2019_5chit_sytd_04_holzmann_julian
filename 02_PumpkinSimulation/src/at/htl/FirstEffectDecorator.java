package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FirstEffectDecorator implements IPumpkinBehaviour {

    IPumpkinBehaviour component;

    @Override
    public int getGrowth(int light, int water) {
        int growthPercentage = light/20;
        return component.getGrowth(light, water) + growthPercentage;
    }
    @Override
    public String getDescription()
    {
        return component.getDescription() + "E1, ";
    }
}
