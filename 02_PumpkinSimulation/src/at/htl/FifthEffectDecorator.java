package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FifthEffectDecorator implements IPumpkinBehaviour {

    IPumpkinBehaviour component;

    @Override
    public int getGrowth(int light, int water) {
        return component.getGrowth(light, water) -1;
    }
    @Override
    public String getDescription()
    {
        return component.getDescription() + "E5, ";
    }
}
