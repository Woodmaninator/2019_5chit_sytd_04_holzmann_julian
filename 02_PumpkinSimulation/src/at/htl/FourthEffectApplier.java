package at.htl;

import java.util.List;

public class FourthEffectApplier implements IEffectApplier {
    @Override
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay) {
        if(inputList.get(currentDay).getWater() >= 50)
            pumpkin = new FourthEffectDecorator(pumpkin);
        return pumpkin;
    }
}
