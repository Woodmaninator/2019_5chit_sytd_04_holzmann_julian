package at.htl;

import java.util.List;

public class FifthEffectApplier implements IEffectApplier {
    @Override
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay) {
        WaterLightPairListAnalyzer analyzer = new WaterLightPairListAnalyzer();
        if(analyzer.getRecentWaterLevel(currentDay,2,inputList) > 30)
            pumpkin = new FifthEffectDecorator(pumpkin);
        return pumpkin;
    }
}
