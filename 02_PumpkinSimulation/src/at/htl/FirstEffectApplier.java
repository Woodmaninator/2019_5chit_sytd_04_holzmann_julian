package at.htl;

import java.util.List;

public class FirstEffectApplier implements IEffectApplier {
    @Override
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay) {
        pumpkin = new FirstEffectDecorator(pumpkin);
        return pumpkin;
    }
}
