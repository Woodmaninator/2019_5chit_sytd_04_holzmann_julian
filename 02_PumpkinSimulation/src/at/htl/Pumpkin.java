package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pumpkin implements IPumpkinBehaviour {
    int baseGrowth;

    @Override
    public int getGrowth(int light, int water) {
        return baseGrowth;
    }

    @Override
    public String getDescription() {
        return "Pumpkin: ";
    }
}
