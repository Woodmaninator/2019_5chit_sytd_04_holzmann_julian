package at.htl;

import java.util.List;

public class ThirdEffectApplier implements IEffectApplier {
    @Override
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay) {
        WaterLightPairListAnalyzer analyzer = new WaterLightPairListAnalyzer();
        if(analyzer.getRecentWaterLevel(currentDay, 10,inputList) < 10)
        {
            pumpkin = new ThirdEffectDecorator(pumpkin);
        }
        return pumpkin;
    }
}
