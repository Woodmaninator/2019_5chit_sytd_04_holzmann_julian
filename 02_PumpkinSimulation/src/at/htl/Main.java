package at.htl;

import javafx.scene.effect.Light;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        startPumpkinSimuation();

    }
    public static void startPumpkinSimuation()
    {
        PumpkinSimulator simulator = new PumpkinSimulator();
        Random r = new Random();
        List<LightWaterPair> inputList = new ArrayList<LightWaterPair>();
        for(int i = 0; i < 100; i++)
        {
            int light = r.nextInt(100);
            inputList.add(new LightWaterPair(light, 100-light));
        }

        List<String> output = simulator.simulate(inputList);

        for(int i = 0; i < output.size(); i++)
            System.out.println(inputList.get(i) + "\n" + output.get(i));
    }
}
