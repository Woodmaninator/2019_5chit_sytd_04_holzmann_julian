package at.htl;

import lombok.NoArgsConstructor;

import java.util.*;
@NoArgsConstructor
public class PumpkinSimulator {
    public List<String> simulate(List<LightWaterPair> inputList)
    {
        List<String> outputList = new ArrayList<String>();
        for(int i = 0; i < inputList.size(); i++ )
        {
            IPumpkinBehaviour pumpkin = new Pumpkin(0);

            //EFFEKT 1 (immer aktiv)
            pumpkin = new FirstEffectApplier().applyEffect(pumpkin,inputList,i);

            //EFFEKT 4 (bei 50% wasser oder mehr)
            pumpkin = new FourthEffectApplier().applyEffect(pumpkin, inputList, i);

            //EFFEKT 5 (30% wasser in den letzten 2 tagen)
            pumpkin = new FifthEffectApplier().applyEffect(pumpkin, inputList,i);

            //EFFEKT 2 (nur wenn 5 tage trocken)
            pumpkin = new SecondEffectApplier().applyEffect(pumpkin,inputList,i);

            //EFFEKT 3 (10 tage trocken)
            pumpkin = new ThirdEffectApplier().applyEffect(pumpkin,inputList, i);

            int growth = pumpkin.getGrowth(inputList.get(i).getLight(), inputList.get(i).getWater());
            String description = pumpkin.getDescription();

            outputList.add("Tag " + i + " : " + description + " | " + growth + " %");

        }
        return outputList;
    }
}
