package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LightWaterPair {
    private Integer light;
    private Integer water;
}
