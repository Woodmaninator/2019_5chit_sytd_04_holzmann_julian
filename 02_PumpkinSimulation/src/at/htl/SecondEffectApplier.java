package at.htl;

import java.util.List;

public class SecondEffectApplier implements IEffectApplier {
    @Override
    public IPumpkinBehaviour applyEffect(IPumpkinBehaviour pumpkin, List<LightWaterPair> inputList, int currentDay) {
        WaterLightPairListAnalyzer analyzer = new WaterLightPairListAnalyzer();
        if(analyzer.getRecentWaterLevel(currentDay,5,inputList) < 10)
        {
            pumpkin = new SecondEffectDecorator(pumpkin);
        }
        return pumpkin;
    }
}
