package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SecondEffectDecorator implements IPumpkinBehaviour {

    IPumpkinBehaviour component;

    @Override
    public int getGrowth(int light, int water) {
        return component.getGrowth(light, water) / 2;
    }
    @Override
    public String getDescription()
    {
        return component.getDescription() + "E2, ";
    }
}
